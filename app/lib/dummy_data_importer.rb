require 'csv'

Struct.new('Package', :name, :license, :vulnerabilities)
Struct.new('Vulnerability', :id, :package_name, :version, :description, :created_at)

class DummyDataImporter
  def self.import!
    self.import_vulnerabilites!
  end

  def self.import_packages!
    {}.tap do |packages|
      CSV.foreach("#{Rails.root}/dummy_data/licenses.csv") do |row|
        package = Struct::Package.new(row[0], row[1], Array.new)
        packages[package.name] = package
      end
    end
  end

  def self.import_vulnerabilites!
    packages = self.import_packages!
    CSV.foreach("#{Rails.root}/dummy_data/vulnerabilities.csv") do |row|
      vul = Struct::Vulnerability.new(row[0], row[1], row[2], row[3], row[4])
      packages[vul.package_name].vulnerabilities << vul
    end
    packages
  end

end
