class NpmPackageReleaseFormatter
  attr_reader :raw_response

  def initialize(raw_response)
    @raw_response = raw_response
  end

  def format
    if valid?
      { name: raw_response['name'],
        latest: versions.last,
        releases: versions
      }
    end
  end

  private

  def versions
    @versions ||= parse_and_sort_versions
  end

  def parse_and_sort_versions
    [].tap do |releases|
      times_and_versions.each do |version_number, time|
        releases.push(version_number)
      end
    end
  end

  def times_and_versions
    raw_response['time'].delete('modified')
    raw_response['time'].delete('created')
    raw_response['time']
  end

  def valid?
    raw_response['name'] && raw_response['time']
  end

end
