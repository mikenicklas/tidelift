class FetchPackageReleases
  attr_reader :package_name, :releases, :raw_response

  def initialize(package_name:)
    @package_name = package_name
  end

  def call
    if fetch_releases_from_npm
      format_releases
    end
  end

  private

  def fetch_releases_from_npm
    @raw_response ||= HTTParty.get(full_uri)
  end

  def full_uri
    "https://registry.npmjs.org/#{package_name}"
  end

  def format_releases
    @releases ||= NpmPackageReleaseFormatter.new(raw_response).format
  end

end
