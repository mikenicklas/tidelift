class PackageHealthController < ApplicationController
  before_action :set_package

  def show
    if @package
      render json: {
        name: @package.name,
        version: params[:version].to_s,
        license: @package.license,
        vulnerabilities: select_vulnerabilities
      }
    else
      render json: {error: 'Package not found.'}, status: 404
    end
  end

  private

  def set_package
    @package = PACKAGE_DATA[params[:package_name]]
  end

  def select_vulnerabilities
    vuls_for_version = @package.vulnerabilities.select do |vul|
      vul.version == params[:version]
    end
    vuls_for_version.map do |vul|
      {id: vul.id, description: vul.description, created: vul.created_at}
    end
  end
end
