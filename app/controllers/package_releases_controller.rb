class PackageReleasesController < ApplicationController
  def show
    client = FetchPackageReleases.new(package_name: params[:package_name])
    client.call
    @releases = client.releases
    if @releases
      render json: @releases
    else
      render json: {error: 'Package not found'}, status: 404
    end
  end
end
