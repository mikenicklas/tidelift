require 'rails_helper'

describe 'Getting package information for a given version', type: :request do
  it 'returns json' do
    get "/package/health/dummy/0.9"
    parsed_response = JSON.parse(response.body)
    expect(parsed_response).to be_truthy
  end
end
