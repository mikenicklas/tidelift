require 'rails_helper'

describe 'Getting package releases', type: :request do

  context 'when package exists' do
    it 'returns formatted json' do
      VCR.use_cassette('package_details', record: :once) do
        expect(FetchPackageReleases).to receive(:new).with(package_name: 'axios').and_call_original
        get "/package/releases/axios"
        parsed_response = JSON.parse(response.body)
        expect(parsed_response['latest']).to be_truthy
        expect(parsed_response['name']).to eql 'axios'
      end
    end
  end

  context 'when package doesnt exist' do
    it 'returns 404 with error message' do
      VCR.use_cassette('package_details_for_non_existing', record: :once) do
        get "/package/releases/not-a-real-package"
        parsed_response = JSON.parse(response.body)
        expect(parsed_response['error']).to be_truthy
        expect(response.status).to eql 404
      end
    end
  end

end
