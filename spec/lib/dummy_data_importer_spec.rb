require 'rails_helper'
require 'pry'

describe DummyDataImporter do
  subject { DummyDataImporter }

  describe '.import' do
    it 'imorts data from csv' do
      dummy_data = DummyDataImporter.import!
      expect(dummy_data['axios']).to be_truthy
    end
  end

end
