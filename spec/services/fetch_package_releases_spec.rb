require 'rails_helper'

describe FetchPackageReleases do
  subject { FetchPackageReleases }

  it 'requires a package_name' do
    expect { subject.new }.to raise_error(ArgumentError)
  end

  it 'does not raise when package_name is given' do
    expect { subject.new(package_name: 'axios') }.not_to raise_error
  end

  describe '#call' do
    context 'when package exists' do

      it 'returns hash of package details' do
        VCR.use_cassette('package_details', record: :once) do
          axios_details = subject.new(package_name: 'axios')
          axios_details.call
          expect(axios_details.releases).to be_truthy
        end
      end

    end

    context 'when package doesnt exist' do
      it 'returns hash of package details' do
        VCR.use_cassette('package_details_for_non_existing', record: :once) do
          axios_details = subject.new(package_name: 'not-a-real-package')
          axios_details.call
          expect(axios_details.releases).to be_falsey
        end
      end
    end
  end

end
