Rails.application.routes.draw do

  get '/package/health/:package_name/:version' => 'package_health#show',
      constraints: { version: /[0-z\.]+/ }
  get '/package/releases/:package_name' => 'package_releases#show'

end
