# Tidelift Challenge

1. Clone this repository
2. Run `bundle install`
3. Run server `bundle exec rails s`


## Tests

This project uses Guard for continuous testing. You can either run tests with Rspec directly by running:

`bundle exec rspec`

Or by using the Guard-Rspec configuration:

`bundle exec guard`

(press enter)
